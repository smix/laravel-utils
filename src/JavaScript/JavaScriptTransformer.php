<?php

namespace SnackMix\Utils\JavaScript;

class JavaScriptTransformer
{
    private $namespace;
    private $binder;

    public function __construct(ViewBinder $binder, $namespace = 'window')
    {
        $this->binder = $binder;
        $this->namespace = $namespace;
    }

    public function put($name, $value)
    {
        $syntax = $this->create($name, $value);
        $this->binder->bind($syntax);
        return $syntax;
    }

    public function render($name, $value)
    {
        $render = $this->create($name, $value);
        return "<script>$render</script>" . PHP_EOL;
    }

    private function create($name, $value)
    {
        return $this->namespace() . $this->variable($name, $value);
    }

    private function namespace()
    {
        if ($this->namespace == 'window') return '';
        return "window.$this->namespace = window.$this->namespace || {};";
    }

    private function variable($key, $value)
    {
        $encoded = $this->encode($value);
        return "$this->namespace.$key = $encoded;";
    }

    private function encode($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}