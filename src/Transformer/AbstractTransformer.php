<?php

namespace SnackMix\Utils\Transformer;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use SnackMix\Utils\CollectionPaginate;

abstract class AbstractTransformer
{
    protected $value;
    protected $options;

    public function __construct($value)
    {
        $this->value = $value;
        $this->options = new Collection;
    }

    public static function make($value)
    {
        return new static($value);
    }

    public function option($name, $value)
    {
        $this->options->put($name, $value);
        return $this;
    }

    public function get()
    {
        if ($this->value == null) {
            return null;
        }
        if ($this->value->count() == 0) {
            return null;
        }
        if ($this->value instanceof Model) {
            return $this->transformOneModel($this->value);
        }
        if ($this->value instanceof Collection) {
            return $this->value->map(function ($item) {
                return $this->transformOneModel($item);
            });
        }
        return null;
    }

    public function paginate($amount, $current)
    {
        if ($this->value instanceof Collection) {
            return CollectionPaginate::make($this->get(), $amount, $current);
        }
        return null;
    }

    private function transformOneModel($model)
    {
        return $this->model($model);
    }

    abstract public function model($model);
}