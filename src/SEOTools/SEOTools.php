<?php

namespace SnackMix\Utils\SEOTools;

class SEOTools
{
    private $title;
    private $title_default;
    private $title_seperator;
    private $description;

    public function __construct()
    {
        $this->title_default = config('app.name');
        $this->title_seperator = ' - ';
    }

    public function generate()
    {
        $contents = '';
        $title = $this->title == null ? $this->title_default : $this->title;
        $contents .= "<title>$title</title>";
        $contents .= sprintf('<meta name="description" content="%s">', $this->description);
        echo html_entity_decode($contents);
        return null;
    }

    public function title($title)
    {
        if ($this->title_default == null) {
            $this->title = $title;
        }
        $this->title = $title . $this->title_seperator . $this->title_default;
        return $this;
    }

    public function titleDefault($title)
    {
        $this->title_default = $title;
        return $this;
    }

    public function description($description)
    {
        $this->description = $description;
        return $this;
    }
}