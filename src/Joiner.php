<?php

namespace SnackMix\Utils;

class Joiner
{
    public static function url()
    {
        $args = func_get_args();
        return collect($args)->map(function ($path) {
            return trim($path, DIRECTORY_SEPARATOR);
        })->implode(DIRECTORY_SEPARATOR);
    }

    public static function path()
    {
        $args = func_get_args();
        $paths = collect($args)->map(function ($path) {
            return trim($path, DIRECTORY_SEPARATOR);
        })->implode(DIRECTORY_SEPARATOR);
        return app()->basePath($paths);
    }
}