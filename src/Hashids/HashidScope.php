<?php

namespace SnackMix\Utils\Hashids;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class HashidScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {

    }

    public function extend(Builder $builder)
    {
        $builder->macro('findHash', function (Builder $builder, $hashid) {
            return $builder->byHashid($hashid)->first();
        });
        $builder->macro('findHashOrFail', function (Builder $builder, $hashid) {
            return $builder->byHashid($hashid)->firstOrFail();
        });
        $builder->macro('byHashid', function (Builder $builder, $hashid) {
            $model = $builder->getModel();
            $key = $model->getKeyName();
            $value = $model->idFromHash($hashid);
            return $builder->where($model->qualifyColumn($key), $value);
        });
    }
}