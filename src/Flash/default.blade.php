<div class="alert alert-{{ $class }}" role="alert">
    @if ($dismissable)
        <button class="close" data-dismiss="alert">&times;</button>
    @endif
    @if (is_null($title) == false)
        <strong>{{ $title }}</strong>
    @endif
    @if (is_array($message))
        <ul class="m-0">
            @foreach ($message as $item)
                <li>{{ $item }}</li>
            @endforeach
        </ul>
    @else
        {{ $message }}
    @endif
</div>