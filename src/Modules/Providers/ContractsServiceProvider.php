<?php
namespace SnackMix\Utils\Modules\Providers;

use Illuminate\Support\ServiceProvider;
use SnackMix\Utils\Modules\Contracts\RepositoryInterface;
use SnackMix\Utils\Modules\Repository;

class ContractsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, Repository::class);
    }
}