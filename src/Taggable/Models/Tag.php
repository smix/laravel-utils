<?php

namespace SnackMix\Utils\Taggable\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'taggable_tags';
    protected $fillable = ['name', 'normalized'];
    public $timestamps = false;

    public function setNameAttribute($value)
    {
        $value = trim($value);
        $this->attributes['name'] = $value;
        $this->attributes['normalized'] = mb_strtolower($value);
    }

    public function __toString()
    {
        return $this->getAttribute('name');
    }

    public static function deleteUnused()
    {
        return (new static)->newQuery()->where('count', '=', 0)->delete();
    }
}
