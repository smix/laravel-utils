<?php
return [
    'delimiters' => ',;',
    'glue' => ',',
    'untag_on_delete' => true,
    'delete_unused_tags' => true
];
