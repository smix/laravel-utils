<?php

namespace SnackMix\Utils\Favicon;

class Favicon
{
    public static function get($value)
    {
        $icon = self::baseUrl($value);
        return file_get_contents($icon);
    }

    private static function baseUrl($url)
    {
        $host = parse_url($url, PHP_URL_HOST);
        return "https://icon.horse/icon/$host";
    }
}
