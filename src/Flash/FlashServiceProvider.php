<?php

namespace SnackMix\Utils\Flash;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class FlashServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('flash', function () {
            return new Notifier();
        });
        $this->app->singleton(NotificationMiddleware::class, function ($app) {
            $notifier = $app->get('flash');
            return new NotificationMiddleware($notifier);
        });
        $this->loadViewsFrom(__DIR__, 'flash');
    }

    public function boot()
    {
        Blade::directive('flashcontainer', function ($container = null) {
            return "<?php echo app('flash')->container($container)->show() ?>";
        });
    }
}