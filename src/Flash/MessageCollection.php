<?php

namespace SnackMix\Utils\Flash;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Collection as BaseCollection;

class MessageCollection extends BaseCollection implements Renderable
{
    public function render()
    {
        $output = '';
        foreach ($this->items as $message) {
            $output .= $message->render();
        }
        return $output;
    }

    public function __toString()
    {
        return $this->render();
    }
}