<?php

namespace SnackMix\Utils\Flash;

use Closure;

class NotificationMiddleware
{
    private $notifier;

    public function __construct(Notifier $notifier)
    {
        $this->notifier = $notifier;
    }

    public function handle($request, Closure $next)
    {
        $containers = session('flashalerts');
        if ($containers == null) return $next($request);
        foreach ($containers as $messages) {
            foreach ($messages as $message) {
                $container = $message->getContainer();
                $this->notifier->container($container)->add($message->getClass(), $message, false);
            }
        }
        session()->forget('flashalerts');
        return $next($request);
    }
}