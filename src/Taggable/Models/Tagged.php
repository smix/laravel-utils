<?php

namespace SnackMix\Utils\Taggable\Models;

use Illuminate\Database\Eloquent\Model;

class Tagged extends Model
{
    protected $table = 'taggable_tagged';
    public $timestamps = false;

    public function taggable()
    {
        return $this->morphTo();
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
