<?php

namespace SnackMix\Utils;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class UtilsServiceProvider extends ServiceProvider
{
    public function register()
    {
        require_once 'helpers.php';
    }

    public function boot()
    {
        Schema::defaultStringLength(200);
        View::addExtension('html', 'php');
    }
}