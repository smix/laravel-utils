<?php

namespace SnackMix\Utils\SEOTools;

use Illuminate\Support\ServiceProvider;

class SEOServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('seotools', function () {
            return new SEOTools();
        });
    }
}