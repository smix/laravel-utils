<?php
function equal($value, $other)
{
    return $value == $other;
}

function not_equal($value, $other)
{
    return equal($value, $other) == false;
}

function not_null($value)
{
    return is_null($value) == false;
}

function json_path($path)
{
    $path = app_path($path);
    $contents = file_get_contents($path);
    return json_decode($contents);
}

function module_path($name)
{
    $module = app('modules')->find($name);
    return $module->getPath();
}