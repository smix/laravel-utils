<?php
namespace SnackMix\Utils\Modules;

use Countable;
use Illuminate\Container\Container;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\Macroable;
use SnackMix\Utils\Modules\Contracts\RepositoryInterface;
use SnackMix\Utils\Modules\Exceptions\ModuleNotFoundException;

class Repository implements RepositoryInterface, Countable
{
    use Macroable;
    private $app;
    private $path;

    public function __construct(Container $app, $path = null)
    {
        $this->app = $app;
        $this->path = $path;
    }

    public function getScanPaths()
    {
        return [$this->getPath()];
    }

    public function scan()
    {
        $modules = collect();
        foreach ($this->getScanPaths() as $path) {
            $manifests = $this->app['files']->glob("{$path}/module.json");
            foreach ($manifests as $manifest) {
                $name = Json::make($manifest)->get('name');
                $modules->put($name, $this->createModule($this->app, $name, dirname($manifest)));
            }
        }
        return $modules;
    }

    public function all()
    {
        return $this->scan();
    }

    public function toCollection()
    {
        return new Collection($this->scan());
    }

    public function getByStatus($status)
    {
        $modules = collect();
        foreach ($this->all() as $name => $module) {
            if ($module->isStatus($status)) {
                $modules->put($name, $module);
            }
        }
        return $modules;
    }

    public function has($name)
    {
        return $this->all()->has($name);
    }

    public function allEnabled()
    {
        return $this->getByStatus(1);
    }

    public function allDisabled()
    {
        return $this->getByStatus(0);
    }

    public function count()
    {
        return count($this->all());
    }

    public function getOrdered($descending = false)
    {
        return $this->allEnabled()->sortBy('order', SORT_REGULAR, $descending);
    }

    public function getPath()
    {
        if ($this->path == null) {
            return $this->config('paths.modules', base_path('modules'));
        }
        return $this->path;
    }

    public function register()
    {
        foreach ($this->getOrdered() as $module) {
            $module->register();
        }
    }

    public function boot()
    {
        foreach ($this->getOrdered() as $module) {
            $module->boot();
        }
    }

    public function find($name)
    {
        foreach ($this->all() as $module) {
            if ($module->getLowerName() == strtolower($name)) {
                return $module;
            }
        }
        return null;
    }

    public function findByAlias($alias)
    {
        foreach ($this->all() as $module) {
            if ($module->getAlias() == $alias) {
                return $module;
            }
        }
        return null;
    }

    public function findRequirements($name)
    {
        $requirements = collect();
        $module = $this->findOrFail($name);
        foreach ($module->getRequires() as $requirement) {
            $requirements->push($this->findByAlias($requirement));
        }
        return $requirements;
    }

    public function findOrFail($name)
    {
        $module = $this->find($name);
        if ($module == null) {
            throw new ModuleNotFoundException("Module {$name} does not exist");
        }
        return $module;
    }

    public function collections($status = 1)
    {
        return new Collection($this->getByStatus($status));
    }

    public function getModulePath($module)
    {
        try {
            return $this->findOrFail($module)->getPath();
        } catch (ModuleNotFoundException $e) {
            return $this->getPath() . '/' . Str::studly($module);
        }
    }

    public function assetPath($module)
    {
        return $this->config('paths.assets') . '/' . $module;
    }

    public function config($key, $default = null)
    {
        return $this->app['config']->get('modules.' . $key, $default);
    }

    public function getFiles()
    {
        return $this->app['files'];
    }

    public function getAssetsPath()
    {
        return $this->config('paths.assets');
    }

    public function enabled($name)
    {
        return $this->findOrFail($name)->enabled();
    }

    public function disabled($name)
    {
        return $this->enabled($name) == false;
    }

    public function enable($name)
    {
        $this->findOrFail($name)->enable();
    }

    public function disable($name)
    {
        $this->findOrFail($name)->disable();
    }

    public function update($module)
    {
        with(new Updater($this))->update($module);
    }

    public function install($name, $version = 'dev-master', $type = 'composer', $subtree = false)
    {
        $installer = new Installer($name, $version, $type, $subtree);
        return $installer->run();
    }

    public function createModule($app, $name, $path)
    {
        return new Module($app, $name, $path);
    }
}