<?php

namespace SnackMix\Utils\Hashids;

use Illuminate\Support\Str;

trait HashidRouting
{
    public function resolveRouteBindingQuery($query, $value, $field = null)
    {
        $field = $this->getRouteKeyName() || $field;
        if ($field == 'hashid' || Str::afterLast($field, '.') == 'hashid') return $query->byHashid($value);
        return parent::resolveRouteBindingQuery($query, $value, $field);
    }

    public function getRouteKey()
    {
        return $this->hashid();
    }

    public function getRouteKeyName()
    {
        return null;
    }
}