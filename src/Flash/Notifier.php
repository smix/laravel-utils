<?php

namespace SnackMix\Utils\Flash;

use Illuminate\Support\Collection;

class Notifier
{
    private $containers;

    public function __construct()
    {
        $this->containers = new Collection;
    }

    public function container($name = 'default')
    {
        if ($this->containers->has($name) == false) {
            $bag = new ContainerBag($name);
            $this->containers->put($name, $bag);
        }
        return $this->containers->get($name);
    }

    public function message($value = null)
    {
        $message = new Message;
        $message->setMessage($value);
        return $message;
    }
}