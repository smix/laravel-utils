<?php

namespace SnackMix\Utils;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CollectionPaginate
{
    public static function make(Collection $data, $amount, $current)
    {
        $items = $data->forPage($current, $amount);
        $paginator = new LengthAwarePaginator($items, $data->count(), $amount, $current);
        return CollectionPaginate::results($paginator);
    }

    public static function transform($transformer, $data, $amount, $current)
    {
        $items = $data->forPage($current, $amount);
        $paginator = new LengthAwarePaginator($transformer::make($items)->get(), $data->count(), $amount, $current);
        return CollectionPaginate::results($paginator);
    }

    private static function results(LengthAwarePaginator $paginator)
    {
        return [
            'data' => $paginator->values(),
            'paginate' => [
                'total' => $paginator->total(),
                'per_page' => $paginator->perPage(),
                'page' => $paginator->currentPage(),
                'next_page' => $paginator->currentPage() + 1,
                'last_page' => $paginator->lastPage(),
                'has_more' => $paginator->hasMorePages()
            ]
        ];
    }
}