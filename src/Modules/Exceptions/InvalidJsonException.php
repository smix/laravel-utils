<?php
namespace SnackMix\Utils\Modules\Exceptions;

use Exception;

class InvalidJsonException extends Exception
{
}