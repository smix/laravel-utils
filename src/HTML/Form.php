<?php

namespace SnackMix\Utils\HTML;

class Form
{
    private $model;
    private $session;
    private $request;
    private $url;
    private $csrfToken;
    private $view;

    public function __construct($request, $view, $url, $session)
    {
        $this->request = $request;
        $this->view = $view;
        $this->url = $url;
        $this->session = $session;
        $this->csrfToken = $session->token();
    }

    public function open()
    {
        return $this;
    }

    public function model($model)
    {
        $this->model = $model;
        return $this->open();
    }

    public function isModel()
    {
        return is_null($this->model) == false;
    }

    public function value($name, $value = null)
    {
        if ($name == null) return $value;
        $session = $this->old($name);
        if (is_null($session) == false) return $session;
        $request = $this->request($name);
        if (is_null($request) == false) return $request;
        if (is_null($value) == false) return $value;
        if (is_null($this->model) == false) return $this->getModelValueAttribute($name);
        return null;
    }

    private function getModelValueAttribute($name)
    {
        if (method_exists($this->model, 'getFormValue')) {
            return $this->model->getFormValue($name);
        }
        return data_get($this->model, $name);
    }

    private function request($name)
    {
        return $this->request->input($name);
    }

    private function old($name)
    {
        return $this->session->getOldInput($name);
    }

    public function close()
    {
        $this->model = null;
    }
}