<?php

namespace SnackMix\Utils\Taggable;

use Illuminate\Database\Eloquent\Model;
use SnackMix\Utils\Taggable\Models\Tag;

class TagService
{
    public function find($tagName)
    {
        $normalized = $this->normalize($tagName);
        return Tag::where('normalized', $normalized)->first();
    }

    public function findOrCreate($tagName)
    {
        $tag = $this->find($tagName);
        if ($tag == false) {
            $tag = Tag::create([
                'name' => $tagName
            ]);
        }
        return $tag;
    }

    public function buildTagArray($tags)
    {
        if (is_array($tags)) {
            return $tags;
        }
        if (is_string($tags)) {
            return preg_split('#[' . preg_quote(config('taggable.delimiters'), '#') . ']#', $tags, null, PREG_SPLIT_NO_EMPTY);
        }
        return (array) $tags;
    }

    public function buildTagArrayNormalized($tags)
    {
        $tags = $this->buildTagArray($tags);
        return array_map([$this, 'normalize'], $tags);
    }

    public function makeTagList(Model $model, $field = 'name')
    {
        $tags = $this->makeTagArray($model, $field);
        return $this->joinList($tags);
    }

    public function joinList(array $array)
    {
        return implode(config('taggable.glue'), $array);
    }

    public function makeTagArray(Model $model, $field = 'name')
    {
        $tags = $model->tags;
        return $tags->pluck($field)->all();
    }

    public function normalize($string)
    {
        return mb_strtolower($string);
    }

    public function getAllTags($class)
    {
        if ($class instanceof Model) {
            $class = get_class($class);
        }
        $sql = 'SELECT DISTINCT t.* FROM taggable_tagged tt LEFT JOIN taggable_tags t ON tt.tag_id=t.id WHERE tt.taggable_type = ?';
        return Tag::fromQuery($sql, [$class]);
    }

    public function incrementCount($tagSlug, $count)
    {
        if ($count < 0) {
            return;
        }
        $tag = Tag::where('normalized', '=', $tagSlug)->first();
        $tag->count = $tag->count + $count;
        $tag->save();
    }

    public function decrementCount($tagSlug, $count)
    {
        if ($count < 0) {
            return;
        }
        $tag = Tag::where('normalized', '=', $tagSlug)->first();
        $tag->count = $tag->count - $count;
        if ($tag->count < 0) {
            $tag->count = 0;
        }
        $tag->save();
    }

    public function deleteUnusedTags()
    {
        return Tag::deleteUnused();
    }
}
