<?php
return [
    'namespace' => 'Modules',
    'paths' => [
        'modules' => base_path('modules'),
        'assets' => public_path('modules'),
        'migration' => base_path('database/migrations')
    ],
    'register' => [
        'translations' => true,
        'files' => 'register'
    ]
];