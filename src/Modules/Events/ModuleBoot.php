<?php
namespace SnackMix\Utils\Modules\Events;

use Illuminate\Queue\SerializesModels;
use SnackMix\Utils\Modules\Module;

class ModuleBoot
{
    use SerializesModels;
    public $module;

    public function __construct(Module $module)
    {
        $this->module = $module;
    }
}