<?php

namespace SnackMix\Utils\JavaScript;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class JavaScriptServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('javascript', function ($app) {
            $binder = new ViewBinder($app->events, 'partials.scripts');
            return new JavaScriptTransformer($binder, 'window');
        });
    }

    public function boot()
    {
        Blade::directive('javascript', function ($expression) {
            return "<?php echo app('javascript')->render($expression) ?>";
        });
    }
}