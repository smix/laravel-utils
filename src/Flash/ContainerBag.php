<?php

namespace SnackMix\Utils\Flash;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class ContainerBag implements Arrayable, Jsonable
{
    private $name;
    private $messages;

    public function __construct($name)
    {
        $this->name = $name;
        $this->messages = new MessageCollection;
    }

    public function add($class, $value, $flash = true)
    {
        if ($value instanceof Message) {
            $value->setClass($class);
            $value->setFlash($flash);
            $value->push($this->messages, $this->name);
        } else {
            $message = new Message($class, $value, $flash);
            $message->push($this->messages, $this->name);
        }
        return $this;
    }

    public function get($class)
    {
        return $this->messages->filter(function ($item) use ($class) {
            return $item->getClass() == $class;
        });
    }

    public function clear($class)
    {
        $filtered = $this->messages->filter(function ($item) use ($class) {
            return not_equal($item->getClass(), $class);
        });
        $this->messages = $filtered;
    }

    public function clearAll()
    {
        $this->messages = new MessageCollection;
        return $this;
    }

    public function all()
    {
        return $this->messages;
    }

    public function count()
    {
        return $this->messages->count();
    }

    public function has($class = null)
    {
        if ($this->count() == 0) {
            return false;
        }
        foreach ($this->messages as $message) {
            if ($message->getClass() == $class) {
                return true;
            }
        }
        return false;
    }

    public function error($message, $flash = true)
    {
        return $this->add('danger', $message, $flash);
    }

    public function success($message, $flash = true)
    {
        return $this->add('success', $message, $flash);
    }

    public function warning($message, $flash = true)
    {
        return $this->add('warning', $message, $flash);
    }

    public function info($message, $flash = true)
    {
        return $this->add('info', $message, $flash);
    }

    public function show($class = null, $format = null)
    {
        $messages = $this->getMessagesRender($class)->sortBy(function ($item) {
            return $item->getPosition();
        });
        $output = '';
        foreach ($messages as $message) {
            if ($message->isFlash() == false) {
                $message->setFormat($format);
                $output .= $message->render();
            }
        }
        return $output;
    }

    private function getMessagesRender($class = null)
    {
        if ($class == null) {
            return $this->messages;
        }
        return $this->get($class);
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'messages' => $this->messages
        ];
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }
}