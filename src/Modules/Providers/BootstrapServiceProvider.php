<?php
namespace SnackMix\Utils\Modules\Providers;

use Illuminate\Support\ServiceProvider;

class BootstrapServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['modules']->boot();
    }

    public function register()
    {
        $this->app['modules']->register();
    }
}