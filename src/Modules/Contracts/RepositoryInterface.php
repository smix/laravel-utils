<?php
namespace SnackMix\Utils\Modules\Contracts;

interface RepositoryInterface
{
    public function all();

    public function scan();

    public function toCollection();

    public function getScanPaths();

    public function allEnabled();

    public function allDisabled();

    public function count();

    public function getOrdered();

    public function getByStatus($status);

    public function find($name);

    public function findOrFail($name);
}