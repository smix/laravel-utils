<?php
namespace SnackMix\Utils\Modules;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Foundation\ProviderRepository;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Support\Traits\Macroable;
use SnackMix\Utils\Modules\Events\ModuleBoot;
use SnackMix\Utils\Modules\Events\ModuleDisabled;
use SnackMix\Utils\Modules\Events\ModuleEnabled;
use SnackMix\Utils\Modules\Events\ModuleRegister;

class Module extends ServiceProvider
{
    use Macroable;
    protected $app;
    private $name;
    private $path;
    private $jsons = [];

    public function __construct($app, $name, $path)
    {
        parent::__construct($app);
        $this->name = $name;
        $this->path = $path;
    }

    public function boot()
    {
        $this->registerTranslation();
        $this->fireEvent(new ModuleBoot($this));
    }

    public function register()
    {
        $this->registerAliases();
        $this->registerProviders();
        $this->registerFiles();
        $this->fireEvent(new ModuleRegister($this));
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLowerName()
    {
        return strtolower($this->name);
    }

    public function getStudlyName()
    {
        return Str::studly($this->name);
    }

    public function getSnakeName()
    {
        return Str::snake($this->name);
    }

    public function getDescription()
    {
        return $this->get('description');
    }

    public function getAlias()
    {
        return $this->get('alias');
    }

    public function getPriority()
    {
        return $this->get('priority');
    }

    public function getRequires()
    {
        return $this->get('requires');
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    private function registerTranslation()
    {
        $lowerName = $this->getLowerName();
        $langPath = $this->getPath() . '/Resources/lang';
        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $lowerName);
        }
    }

    private function registerProviders()
    {
        $repo = new ProviderRepository($this->app, new Filesystem, $this->getCachedServicesPath());
        $repo->load($this->get('providers'));
    }

    private function registerAliases()
    {
        $loader = AliasLoader::getInstance();
        foreach ($this->get('aliases') as $alias) {
            $loader->alias($alias['name'], $alias['class']);
        }
    }

    private function getCachedServicesPath()
    {
        return Str::replaceLast('services.php', $this->getSnakeName() . '_module.php', $this->app->getCachedServicesPath());
    }

    private function registerFiles()
    {
        foreach ($this->get('files') as $file) {
            include $this->path . '/' . $file;
        }
    }

    private function fireEvent($event)
    {
        $this->app['events']->fire($event);
    }

    public function isStatus($status)
    {
        return $this->get('active', 0) == $status;
    }

    public function enabled()
    {
        return $this->isStatus(1);
    }

    public function disabled()
    {
        return $this->enabled() == false;
    }

    public function setActive($active)
    {
        return $this->json()->set('active', $active)->save();
    }

    public function disable()
    {
        $this->setActive(0);
        $this->fireEvent(new ModuleDisabled($this));
    }

    public function enable()
    {
        $this->setActive(1);
        $this->fireEvent(new ModuleEnabled($this));
    }

    public function getExtraPath($path)
    {
        return $this->getPath() . '/' . $path;
    }

    public function json($file = null)
    {
        if ($file == null) {
            $file = 'module.json';
        }
        if (array_key_exists($file, $this->jsons) == false) {
            $this->jsons[$file] = new Json($this->getPath() . '/' . $file);
        }
        return $this->jsons[$file];
    }

    public function get($key, $default = null)
    {
        return $this->json()->get($key, $default);
    }

    public function getComposerAttr($key, $default = null)
    {
        return $this->json('composer.json')->get($key, $default);
    }

    public function __toString()
    {
        return $this->getStudlyName();
    }

    public function __get($key)
    {
        return $this->get($key);
    }
}