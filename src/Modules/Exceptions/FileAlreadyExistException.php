<?php
namespace SnackMix\Utils\Modules\Exceptions;

use Exception;

class FileAlreadyExistException extends Exception
{
}