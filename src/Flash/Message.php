<?php

namespace SnackMix\Utils\Flash;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Renderable;

class Message implements Arrayable, Jsonable, Renderable
{
    private $message;
    private $class;
    private $dismissable = true;
    private $title = null;
    private $format = null;
    private $flash;
    private $position = 0;
    private $container = null;

    public function __construct($type = null, $message = null, $flash = true)
    {
        $this->class = $type;
        $this->message = $message;
        $this->flash = $flash;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function setDismissable($dismissable)
    {
        $this->dismissable = $dismissable;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function isFlash()
    {
        return $this->flash;
    }

    public function setFlash($flash)
    {
        $this->flash = $flash;
        return $this;
    }

    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function push($messages, $name)
    {
        $this->setContainer($name);
        if ($this->flash) {
            session()->push("flashalerts.$name", $this);
        } else {
            $messages->push($this);
        }
    }

    public function getData()
    {
        return [
            'message' => $this->message,
            'dismissable' => $this->dismissable,
            'title' => $this->title,
            'class' => $this->class
        ];
    }

    public function render()
    {
        $data = $this->getData();
        if ($this->format == null) {
            return view('flash::default', $data)->render();
        }
        return view($this->format, $data)->render();
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray(), $options);
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function __toString()
    {
        return $this->render();
    }
}