<?php
namespace SnackMix\Utils\Modules\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class DisableCommand extends Command
{
    protected $name = 'module:disable {module}';
    protected $description = 'Disable the specified module.';

    public function handle()
    {
        $module = $this->laravel['modules']->findOrFail($this->argument('module'));
        if ($module->enabled()) {
            $module->disable();
            $this->info("Module {$module} disabled successful.");
        } else {
            $this->comment("Module {$module} has already disabled.");
        }
    }
}