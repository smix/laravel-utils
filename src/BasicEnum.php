<?php

namespace SnackMix\Utils;

use ReflectionClass;

abstract class BasicEnum
{
    private static $cache = [];

    public static function getConstants()
    {
        $class = get_called_class();
        if (array_key_exists($class, self::$cache) == false) {
            $reflect = new ReflectionClass($class);
            self::$cache[$class] = $reflect->getConstants();
        }
        return self::$cache[$class];
    }

    public static function isName($name, $strict = false)
    {
        $constants = self::getConstants();
        if ($strict) return array_key_exists($name, $constants);
        $keys = array_keys($constants);
        $mapped = array_map('strtolower', $keys);
        $name = strtolower($name);
        return in_array($name, $mapped);
    }

    public static function isValue($value, $strict = true)
    {
        $constants = self::getConstants();
        $values = array_values($constants);
        return in_array($value, $values, $strict);
    }
}