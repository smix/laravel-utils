<?php
namespace SnackMix\Utils\Modules\Contracts;

interface RunableInterface
{
    public function run($command);
}