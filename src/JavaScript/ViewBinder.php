<?php

namespace SnackMix\Utils\JavaScript;

use Illuminate\Contracts\Events\Dispatcher;

class ViewBinder
{
    private $result = '';

    function __construct(Dispatcher $event, $view)
    {
        $event->listen("composing: $view", function () {
            echo "<script>$this->result</script>" . PHP_EOL;
        });
    }

    public function bind($content)
    {
        $this->result .= $content;
    }
}