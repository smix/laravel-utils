<?php

namespace SnackMix\Utils\Hashids;

trait HasHashid
{
    public static function bootHasHashid()
    {
        static::addGlobalScope(new HashidScope);
    }

    public function hashid()
    {
        $key = $this->getKey();
        return $this->hashFromId($key);
    }

    public function idFromHash($hashid)
    {
        $data = str_replace($this->getHashIdsPrefix(), '', $hashid);
        $ids = app('hashids')->decode($data);
        if (count($ids) == 0) return null;
        return $ids[0];
    }

    public function hashFromId($id)
    {
        $hash = app('hashids')->encode($id);
        return $this->getHashIdsPrefix() . $hash;
    }

    protected function getHashidAttribute()
    {
        return $this->hashid();
    }

    public function getHashIdsPrefix()
    {
        return '';
    }
}