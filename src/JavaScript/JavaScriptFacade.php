<?php

namespace SnackMix\Utils\JavaScript;

use Illuminate\Support\Facades\Facade;

class JavaScriptFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'javascript';
    }
}