<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaggableTable extends Migration
{
    public function up()
    {
        Schema::create('taggable_tags', function (Blueprint $table) {
            $table->increments('tag_id');
            $table->string('name');
            $table->string('normalized');
            $table->boolean('suggest')->default(false);
            $table->integer('count')->unsigned()->default(0);
            $table->timestamps();
        });
        Schema::create('taggable_tagged', function (Blueprint $table) {
            $table->unsignedInteger('tag_id');
            $table->unsignedInteger('taggable_id');
            $table->string('taggable_type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('taggable_tags');
        Schema::drop('taggable_taggables');
    }
}
