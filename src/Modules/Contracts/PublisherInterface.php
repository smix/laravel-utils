<?php
namespace SnackMix\Utils\Modules\Contracts;

interface PublisherInterface
{
    public function publish();
}