<?php
namespace SnackMix\Utils\Modules\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class EnableCommand extends Command
{
    protected $name = 'module:enable {module}';
    protected $description = 'Enable the specified module.';

    public function handle()
    {
        $module = $this->laravel['modules']->findOrFail($this->argument('module'));
        if ($module->disabled()) {
            $module->enable();
            $this->info("Module {$module} enabled successful.");
        } else {
            $this->comment("Module {$module} has already enabled.");
        }
    }
}