<?php
namespace SnackMix\Utils\Modules\Exceptions;

use Exception;

class ModuleNotFoundException extends Exception
{
}