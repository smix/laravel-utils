<?php
namespace SnackMix\Utils\Modules\Commands;

use Illuminate\Console\Command;
use MatthiasMullie\PathConverter\Converter as PathConverter;
use SnackMix\Utils\Modules\Facades\Module;

class BundleAssets extends Command
{
    protected $signature = 'module:bundle-assets';
    protected $description = 'Generates sass and js files bundled from index files in modules';
    private $js;
    private $sass;
    private $npmdeps = [];

    public function handle()
    {
        Module::allEnabled()->each(function (Module $module) {
            $this->sass($module);
            $this->js($module);
            $this->npm($module);
        });
        $this->save('assets/sass/modules.scss', $this->sass);
        $this->save('assets/js/modules.js', $this->js);
        if (file_exists(resource_path('installed-modules')) == false) {
            mkdir(resource_path('installed-modules'), 0755, true);
        }
        $npm = [
            'name' => 'installed-modules',
            'version' => '0.0.1',
            'private' => true,
            'dependencies' => $this->npmdeps
        ];
        $this->save('installed-modules/package.json', json_encode($npm, JSON_PRETTY_PRINT));
        $this->info('modules.scss, modules.js and package.json have been generated.');
    }

    private function sass($module)
    {
        $index = 'Resources/assets/sass/index.scss';
        $path = new PathConverter($module->getPath(), resource_path('assets/sass/modules.scss'));
        if (file_exists($module->getPath() . '/' . $index)) {
            $this->sass .= '@import "' . $path->convert($index) . '";' . PHP_EOL;
        }
    }

    private function js($module)
    {
        $index = 'Resources/assets/js/index.js';
        $path = new PathConverter($module->getPath(), resource_path('assets/js/modules.js'));
        if (file_exists($module->getPath() . '/' . $index)) {
            $this->js .= "require('" . $path->convert($index) . "');" . PHP_EOL;
        }
    }

    private function npm($module)
    {
        $index = '/npm/package.json';
        $path = new PathConverter($module->getPath(), resource_path('installed-modules/package.json'));
        if (file_exists($module->getPath() . $index)) {
            $package = json_decode(file_get_contents($module->getPath() . $index));
            $this->npmdeps[$package->name] = 'file:' . $path->convert('npm');
        }
    }

    private function save($file, $content)
    {
        file_put_contents(resource_path($file), $content);
    }
}