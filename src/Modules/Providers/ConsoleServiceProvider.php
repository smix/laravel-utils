<?php

namespace SnackMix\Utils\Modules\Providers;

use Illuminate\Support\ServiceProvider;
use SnackMix\Utils\Modules\Commands\BundleAssets;
use SnackMix\Utils\Modules\Commands\DisableCommand;
use SnackMix\Utils\Modules\Commands\EnableCommand;
use SnackMix\Utils\Modules\Commands\ListCommand;

class ConsoleServiceProvider extends ServiceProvider
{
    protected $commands = [
        BundleAssets::class,
        DisableCommand::class,
        EnableCommand::class,
        ListCommand::class
    ];

    public function register()
    {
        $this->commands($this->commands);
    }

    public function provides()
    {
        return $this->commands;
    }
}