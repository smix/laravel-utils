<?php

namespace SnackMix\Utils\Taggable;

use Illuminate\Support\ServiceProvider;

class TaggableServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/taggable.php', 'taggable');
        $this->app->singleton(TagService::class);
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
    }
}
