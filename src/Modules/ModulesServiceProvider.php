<?php

namespace SnackMix\Utils\Modules;

use Illuminate\Support\ServiceProvider;
use SnackMix\Utils\Modules\Providers\BootstrapServiceProvider;
use SnackMix\Utils\Modules\Providers\ConsoleServiceProvider;
use SnackMix\Utils\Modules\Providers\ContractsServiceProvider;

class ModulesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->register(BootstrapServiceProvider::class);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/modules.php', 'modules');
        $this->registerServices();
        $this->registerProviders();
    }

    private function registerServices()
    {
        $this->app->singleton('modules', function ($app) {
            $path = $app['config']->get('modules.paths.modules');
            return new Repository($app, $path);
        });
    }

    private function registerProviders()
    {
        $this->app->register(ConsoleServiceProvider::class);
        $this->app->register(ContractsServiceProvider::class);
    }

    public function provides()
    {
        return ['modules'];
    }
}