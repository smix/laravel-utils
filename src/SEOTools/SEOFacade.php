<?php

namespace SnackMix\Utils\SEOTools;

use Illuminate\Support\Facades\Facade;

class SEOFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'seotools';
    }
}