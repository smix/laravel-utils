<?php

namespace SnackMix\Utils\Taggable;

use Illuminate\Database\Eloquent\Builder;
use SnackMix\Utils\Taggable\Models\Tag;

trait Taggable
{
    public static function bootTaggable()
    {
        if (static::untagOnDelete()) {
            static::deleting(function ($model) {
                $model->detag();
            });
        }
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable', 'taggable_tagged', 'taggable_id', 'tag_id');
    }

    public function tag($tags)
    {
        $tags = app(TagService::class)->buildTagArray($tags);
        foreach ($tags as $tagName) {
            $this->addOneTag($tagName);
        }
        return $this->load('tags');
    }

    public function untag($tags = null)
    {
        if (is_null($tags)) {
            $tags = $this->tagArray;
        }
        $tags = app(TagService::class)->buildTagArray($tags);
        foreach ($tags as $tagName) {
            $this->removeOneTag($tagName);
        }
        if (static::shouldDeleteUnused()) {
            app(TagService::class)->deleteUnusedTags();
        }
        return $this->load('tags');
    }

    public static function untagOnDelete()
    {
        return config('taggable.untag_on_delete');
    }

    public static function shouldDeleteUnused()
    {
        return config('taggable.delete_unused_tags');
    }

    public function retag($tags)
    {
        return $this->detag()->tag($tags);
    }

    public function detag()
    {
        return $this->untag();
    }

    protected function addOneTag($tagName)
    {
        $tagSlug = mb_strtolower($tagName);
        $tag = app(TagService::class)->findOrCreate($tagName);
        if ($this->tags->contains($tag->getKey()) == false) {
            $this->tags()->attach($tag->getKey());
            app(TagService::class)->incrementCount($tagSlug, 1);
        }
    }

    protected function removeOneTag($tagName)
    {
        $tagSlug = mb_strtolower($tagName);
        $tag = app(TagService::class)->find($tagName);
        if ($tag) {
            $this->tags()->detach($tag);
            app(TagService::class)->decrementCount($tagSlug, 1);
        }
    }

    public function getTagListAttribute()
    {
        return app(TagService::class)->makeTagList($this);
    }

    public function getTagListNormalizedAttribute()
    {
        return app(TagService::class)->makeTagList($this, 'normalized');
    }

    public function getTagArrayAttribute()
    {
        return app(TagService::class)->makeTagArray($this);
    }

    public function getTagArrayNormalizedAttribute()
    {
        return app(TagService::class)->makeTagArray($this, 'normalized');
    }

    public function scopeWithAllTags(Builder $query, $tags)
    {
        $normalized = app(TagService::class)->buildTagArrayNormalized($tags);
        return $query->has('tags', '=', count($normalized), 'and', function (Builder $builder) use ($normalized) {
            $builder->whereIn('normalized', $normalized);
        });
    }

    public function scopeWithAnyTags(Builder $query, $tags = [])
    {
        $normalized = app(TagService::class)->buildTagArrayNormalized($tags);
        if (empty($normalized)) {
            return $query->has('tags');
        }
        return $query->has('tags', '>', 0, 'and', function (Builder $builder) use ($normalized) {
            $builder->whereIn('normalized', $normalized);
        });
    }

    public function scopeWithoutTags(Builder $query)
    {
        return $query->has('tags', '=', 0);
    }

    public static function allTags()
    {
        $tags = app(TagService::class)->getAllTags(get_called_class());
        return $tags->pluck('name')->sort()->all();
    }

    public static function allTagsList()
    {
        return app(TagService::class)->joinList(static::allTags());
    }
}
