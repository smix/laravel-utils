<?php

namespace SnackMix\Utils\Modules;

use Illuminate\Filesystem\Filesystem;

class Json
{
    private $path;
    private $filesystem;
    private $attributes;

    public function __construct($path)
    {
        $this->path = $path;
        $this->filesystem = new Filesystem;
        $attributes = $this->getAttributes();
        $this->attributes = Collection::make($attributes);
    }

    public static function make($path)
    {
        return new static($path);
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    public function getContents()
    {
        $path = $this->getPath();
        return $this->filesystem->get($path);
    }

    public function getAttributes()
    {
        return json_decode($this->getContents(), true);
    }

    public function toJsonPretty($data = null)
    {
        if ($data == null) {
            return json_encode($this->attributes, JSON_PRETTY_PRINT);
        }
        return json_encode($data, JSON_PRETTY_PRINT);
    }

    public function update(array $data)
    {
        $attributes = $this->attributes->toArray();
        $merged = array_merge($attributes, $data);
        $this->attributes = new Collection($merged);
        return $this->save();
    }

    public function set($key, $value)
    {
        $this->attributes->offsetSet($key, $value);
        return $this;
    }

    public function save()
    {
        $json = $this->toJsonPretty();
        return $this->filesystem->put($this->getPath(), $json);
    }

    public function get($key, $default = null)
    {
        return $this->attributes->get($key, $default);
    }

    public function __toString()
    {
        return $this->getContents();
    }
}