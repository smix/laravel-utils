<?php

namespace SnackMix\Utils;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use ReflectionClass;

class Lara
{
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NO_CONTENT = 204;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_CONFLICT = 409;
    const HTTP_UNPROCESSABLE_ENTITY = 422;
    const HTTP_INTERNAL_SERVER_ERROR = 500;
    const HTTP_NOT_IMPLEMENTED = 501;
    const HTTP_SERVICE_UNAVAILABLE = 503;
    private $data;
    private $status;

    public function __construct($status)
    {
        $this->status = $status;
        $this->data = new Collection;
    }

    public function with($name, $value)
    {
        $this->data->put($name, $value);
        return $this;
    }

    public function send()
    {
        return response()->json($this->data, $this->status);
    }

    private static function getStatusName($status)
    {
        $reflect = new ReflectionClass(Lara::class);
        $constants = $reflect->getConstants();
        return array_search($status, $constants);
    }

    public static function getSuccessResponse($send, $status)
    {
        $container = new Lara($status);
        $container->with('success', true);
        return $send ? $container->send() : $container;
    }

    public static function getErrorResponse($message, $status)
    {
        $container = new Lara($status);
        $error = $message == null ? self::getStatusName($status) : $message;
        $container->with('error', $error);
        return $container->send();
    }

    public static function ok($send = true)
    {
        return self::getSuccessResponse($send, self::HTTP_OK);
    }

    public static function created($send = true)
    {
        return self::getSuccessResponse($send, self::HTTP_CREATED);
    }

    public static function accepted($send = true)
    {
        return self::getSuccessResponse($send, self::HTTP_ACCEPTED);
    }

    public static function noContent($send = true)
    {
        return self::getSuccessResponse($send, self::HTTP_NO_CONTENT);
    }

    public static function badRequest($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_BAD_REQUEST);
    }

    public static function unauthorized($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_UNAUTHORIZED);
    }

    public static function forbidden($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_FORBIDDEN);
    }

    public static function notFound($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_NOT_FOUND);
    }

    public static function methodNotAllowed($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_METHOD_NOT_ALLOWED);
    }

    public static function conflict($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_CONFLICT);
    }

    public static function unprocessableEntity($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_UNPROCESSABLE_ENTITY);
    }

    public static function internalError($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_INTERNAL_SERVER_ERROR);
    }

    public static function exception(Exception $exception)
    {
        Log::error($exception);
        $message = $exception->getMessage();
        return self::internalError($message);
    }

    public static function notImplemented($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_NOT_IMPLEMENTED);
    }

    public static function notAvailable($message = null)
    {
        return self::getErrorResponse($message, self::HTTP_SERVICE_UNAVAILABLE);
    }
}