<?php

namespace SnackMix\Utils\HTML;

use Illuminate\Support\ServiceProvider;

class FormProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('form', function ($app) {
            $session = $app->get('session.store');
            return new Form($app->request, $app->view, $app->url, $session);
        });
    }
}