<?php

namespace SnackMix\Utils;

use Illuminate\Database\Eloquent\Model;

trait OwnsModels
{
    public function owns(Model $model, $foreign = null)
    {
        $key = $foreign == null ? $this->getForeignKey() : $foreign;
        return $this->getKey() == $model->getAttribute($key);
    }

    public function doesntOwn(Model $model)
    {
        return $this->owns($model) == false;
    }
}