<?php

namespace SnackMix\Utils\Hashids;

use Hashids\Hashids;
use Illuminate\Support\ServiceProvider;

class HashServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('hashids', function () {
            $salt = env('HASHID_SALT', '');
            $length = env('HASHID_LEN', 0);
            return new Hashids($salt, $length);
        });
    }
}