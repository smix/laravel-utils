<?php
namespace SnackMix\Utils\Modules\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class ListCommand extends Command
{
    protected $name = 'module:list';
    protected $description = 'Show list of all modules.';

    public function handle()
    {
        $this->table(['Name', 'Status', 'Order', 'Path'], $this->getRows());
    }

    public function getRows()
    {
        $rows = [];
        foreach ($this->getModules() as $module) {
            $rows[] = [
                $module->getName(),
                $module->enabled() ? 'Enabled' : 'Disabled',
                $module->get('order'),
                $module->getPath(),
            ];
        }
        return $rows;
    }

    public function getModules()
    {
        switch ($this->option('only')) {
            case 'enabled':
                return $this->laravel['modules']->getByStatus(1);
            case 'disabled':
                return $this->laravel['modules']->getByStatus(0);
            case 'ordered':
                return $this->laravel['modules']->getOrdered($this->option('descending'));
            default:
                return $this->laravel['modules']->all();
        }
    }

    protected function getOptions()
    {
        return [
            ['only', null, InputOption::VALUE_OPTIONAL, 'Types of modules will be displayed.', null],
            ['descending', 'd', InputOption::VALUE_OPTIONAL, 'The direction of ordering.', false]
        ];
    }
}