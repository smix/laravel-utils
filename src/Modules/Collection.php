<?php

namespace SnackMix\Utils\Modules;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection as BaseCollection;

class Collection extends BaseCollection
{
    public function getItems()
    {
        return $this->items;
    }

    public function toArray()
    {
        return array_map(function ($value) {
            if ($value instanceof Module) {
                $attributes = $value->json()->getAttributes();
                $attributes['path'] = $value->getPath();
                return $attributes;
            } else if ($value instanceof Arrayable) {
                return $value->toArray();
            }
            return $value;
        }, $this->items);
    }
}